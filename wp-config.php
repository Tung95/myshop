<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define( 'DB_NAME', 'eshop' );

/** Username của database */
define( 'DB_USER', 'root' );

/** Mật khẩu của database */
define( 'DB_PASSWORD', '' );

/** Hostname của database */
define( 'DB_HOST', 'localhost' );

/** Database charset sử dụng để tạo bảng database. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         't^|kp*Xg@pS{2ZX0S[$-4#@I^&.w=::U5R7sD>XR2Y+DH`=8)aMiY&/Va::o;r{!' );
define( 'SECURE_AUTH_KEY',  'NS6/]V9$/dERQUXAZ3_no*B@@=`V*pMP9-FC}B[>${0&O4*Trdv}%fmgyz*6jGI?' );
define( 'LOGGED_IN_KEY',    '!h@$U6(](8w.Tm?i0Rl}4_dz.(<5GL|)aK=we-X_y6r b$Ej5k,RXmncSF{bzM0*' );
define( 'NONCE_KEY',        'z<-wLoqDN`+s@QE_bN7VTT6NDydAmr%&hp9ZZBRfJ5F?DSK,iD/5U~Vfd,]IU8bP' );
define( 'AUTH_SALT',        'i,7b$-tb]Df]L<@*rZ ONXzb}L@r2;3Rn=@,=KH-a1ZW@% _OP+kH*a,fJpd!e*q' );
define( 'SECURE_AUTH_SALT', '}10@uVa^p{W7WG]6a*f6fqLVtG>&H;j|Qf8i^97a|cykVF3a-5%]/  V!YZKZNVu' );
define( 'LOGGED_IN_SALT',   'oO)eUd=l6N1Anw7HfE!vU{3RVqM[s|r7sv^3w!**0L_K[K|H~yA$$-D_=ZrqTptA' );
define( 'NONCE_SALT',       '_5pkFN=2QA1R.[BO=@,[[_nW6($_U0u=B7#jM-m %`PekujaELs=VqamI;uq.-ai' );

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix  = 'wp_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
